FROM python:3

WORKDIR /usr/src/app

COPY requirements.txt ./
RUN pip install --no-cache-dir -r requirements.txt

VOLUME /var/fix-data

COPY . .

# Define environment variable
ENV SRC_FILE 'uber.csv'
ENV DST_DIR .

CMD python ./class-ex.py -src_file ./uber.csv -dst_dir .
